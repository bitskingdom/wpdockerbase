**Docker**
#Run on terminal
docker-compose up

#Run Deattached
docker-compose up -d 

#Run specifc docker file
docker-compose -f docker-compose-file.yml up -d 

#Down
docker-compose down

#Down and remove volumes
docker-compose down -v

#Down and remove volumes --remove-orphans
docker-compose down -v --remove-orphans


**CLI**
docker-compose -f docker-compose-cli.yml up -d

#Set Perimissions
docker exec -i mysite_wordpress sh -c 'chown www-data:www-data -R wp-content'

#Initial Setup
docker exec -i cli sh -c "wp core install --url=localhost:8888 --title=MySite --admin_user=admin --admin_password=admin --admin_email=admin@domain.com"

#Install Plugins

#Custom
docker exec -i cli sh -c "wp plugin install customPlugins/woocommerce.3.7.0.zip --force --allow-root --activate"

#from WP Repo
docker exec -i cli sh -c "wp plugin install contact-form-7 --activate"