#!/bin/bash

RED="\033[1;31m"
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

SSH_PARAMS="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

# Export the vars in .env into your shell:
echo -e ${GREEN}"-- BUILDING With .env file --"${NOCOLOR}
export $(egrep -v '^#' .env | xargs)

        echo -e ${GREEN}"1 - Server Deploy 🚀 - "${NOCOLOR}

        echo -e ${GREEN}"1.1 ---- Check Docker On Server ----"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "type docker > /dev/null 2>& 1 && echo 'Docker present.' || ( echo 'Installing Docker.' ; wget -qO- https://get.docker.com/ | sh; sudo usermod -aG docker $(whoami); sudo apt-get -y install python-pip; sudo pip install docker-compose )"

        echo -e ${GREEN}"1.2 ---- Create project Folder On Server ---"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "mkdir -p $SERVER_FOLDER"

        echo -e ${GREEN}"1.3 ---- Copy Files To Server  - " ${NOCOLOR}
        rsync -azI -e "ssh -i $SERVER_SSH_KEY $SSH_PARAMS" --exclude ".git" $LOCAL_PATH/ $SERVER_SSH_USER@$SERVER_IP:$SERVER_FOLDER

        echo -e ${GREEN}"1.4 ---- Stop Docker on Server ---"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cd ~/$SERVER_FOLDER; docker-compose down"

        echo -e ${GREEN}"1.5 ---- Run Build On Server  - "${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "cd ~/$SERVER_FOLDER; docker-compose -f docker-compose_production.yml up -d"
        
        echo -e ${GREEN}"1.6 ---- Set permissions"${NOCOLOR}
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "docker exec -i "${APPNAME}"_wordpress sh -c 'chown www-data:www-data -R wp-content'"
        
        echo -e ${GREEN}"1.7 ---- Check MySql"${NOCOLOR}

        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP "type mysql > /dev/null 2>& 1 && echo 'MySQL present.' || ( echo 'Installing MySQL.' ; apt install mysql-server -y )"

        if [ "$INSTALL" == "true" ]
                then
                echo -e ${GREEN}"1.8 ---- Initial Setup"${NOCOLOR}
                ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'mysql --user='${DBUSER}' --password="'${DBPASS}'" --port=3306 --host=127.0.0.1 --database='${DBNAME}'_wp_db < '$SERVER_FOLDER'/db/wp_db.sql'
                ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp core install --url=${SERVER_IP} --title=MySite --admin_user=admin --admin_password=admin --admin_email=admin@domain.com"'
                echo -e ${GREEN}"1.9 ---- DB Update"${NOCOLOR}
                ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp search-replace 'localhost' '${SERVER_IP}' --all-tables"'

        fi

        echo -e ${GREEN}"2.0 ---- Initial Plugins"${NOCOLOR}
        #Custom Plugins:
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp plugin install customPlugins/woocommerce.3.7.0.zip --force --allow-root --activate"'
        #Wordpress Repo Plugins
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp plugin install contact-form-7 --activate"'
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp plugin install safe-svg --activate"'
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp plugin install wp-super-cache --activate"'
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp plugin install all-in-one-seo-pack --activate"'
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp plugin install really-simple-ssl --activate"'
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp plugin install wp-mail-smtp --activate"'
        ssh -i $SERVER_SSH_KEY $SSH_PARAMS $SERVER_SSH_USER@$SERVER_IP 'docker exec -i cli sh -c "wp plugin install wordfence --activate"'

