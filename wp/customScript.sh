#!/bin/bash

##Set execute Perimissions to this file first: chmod +x customScript.sh

echo "My Custom Script 🚀"
#docker-compose down -v
#docker-compose down
#docker-compose up
#docker-compose up -d
docker-compose -f docker-compose-cli.yml up -d

##Set Perimissions
docker exec -i mysite_wordpress sh -c 'chown www-data:www-data -R wp-content'

#Initial Setup
docker exec -i cli sh -c "wp core install --url=localhost:8888 --title=MySite --admin_user=admin --admin_password=admin --admin_email=admin@domain.com"

##Install Plugins

##Custom

docker exec -i cli sh -c "wp plugin install customPlugins/woocommerce.3.7.0.zip --force --allow-root --activate"

##from WP Repo

docker exec -i cli sh -c "wp plugin install contact-form-7 --activate"